# Stock System
    This project is part of a test for a software engineer position. 
    There is a PDF file (StockHandlingTask.pdf) that explains the challenge.

## Architecture
    The project is a back-end server that provides a REST interface 
    for expose the services.
* Stack: 
    * Spring-Boot 1.5.7
    * Spring-Data 1.5.7
    * Java 8
    * H2 Database(embedded)
    * TomCat Server(embedded)
## Test Structure
## Unit Test & Coverage
	It was defined, using JaCoCo plugin, the check point of coverage in 85%,
	current is 93%.
### How to Generate Cover Report
    mvn clean test jacoco:report
    firefox target/site/jacoco/index.html

## How to Run
### Requirements
* Java 8 - JDK
* Maven

### Execution Commands
    mvn package
    java -jar target/stock.jar
* After this, the services could be accessed in your browser.

## Examples of use
    It was provided some initial data like products, order registers 
    and stocks. You can check in the end of this page.

### GET /stock?productId=<productId>
    http://localhost:8080/stock?productId=vegetable-123

### GET /statistics?time[today,lasMonth]
    http://localhost:8080/statistics?time=today
    http://localhost:8080/statistics?time=lastMonth

### POST /updateStock
    curl -X POST -H 'content-type: application/json' -d '{"id":"01234", "quantity":1000111, "productId":"vegetable-123", "timestamp":"2017-12-16 22:54:01" }' http://localhost:8080/updateStock/

## Initial DataSet
    
### Products
| Product Id    | Date      |
|:--------------|:---------:|    	
| vegetable-123 | new Date()|
| car-54        | new Date()|
| tire-11       | new Date()|
| apple-45      | new Date()|
| plane-99      | new Date()|
| egg-131       | new Date()|

### Stocks
| Stock Id      | Product Id    | Quantity  |   Date    |
|:--------------|:------------- |----------:|:---------:|
|000001         | vegetable-123 | 100       | new Date()|
|121346         | car-54        | 1200      | new Date()|
|321346         | tire-11       | 11        | new Date()|
|445566         | apple-45      | 1         | new Date()|
|677657         | plane-99      | 1000      | new Date()|

#### Product Orders Today
| Product Id      | Order Quantity | Date      |
|:--------------- |---------------:|:---------:|
|vegetable-123    | 20             | new Date()|
|vegetable-123    | 11             | new Date()|
|vegetable-123    | 67             | new Date()|
|car-54           | 617            | new Date()|
|car-54           | 27             | new Date()|
|tire-11          | 1              | new Date()|
|tire-11          | 1              | new Date()|
|plane-99         | 167            | new Date()|

### Product Orders Last Month (Day 15 of last month)
| Product Id      | Order Quantity |      Date      |
|:--------------- |---------------:|:--------------:|
|vegetable-123    | 5              | LastMonth/15/17|
|car-54           | 58             | LastMonth/15/17|
|tire-11          | 85             | LastMonth/15/17|
|apple-45         | 11             | LastMonth/15/17|
