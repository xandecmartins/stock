package com.commercetools.stock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.commercetools.stock.service.StatisticService;
import com.commercetools.stock.service.StatisticServiceImpl;
import com.commercetools.stock.service.StockService;
import com.commercetools.stock.service.StockServiceImpl;

@Configuration
public class AppConfig {
	
	@Bean
	public StockService stockService(){
		return new StockServiceImpl();
	}
	
	@Bean
	public StatisticService statisticsService(){
		return new StatisticServiceImpl();
	}
}