package com.commercetools.stock;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.commercetools.stock.domain.Product;
import com.commercetools.stock.domain.ProductOrder;
import com.commercetools.stock.domain.Stock;
import com.commercetools.stock.repository.ProductRepository;
import com.commercetools.stock.repository.ProductOrderRepository;
import com.commercetools.stock.repository.StockRepository;

@Component
public class DataLoader implements ApplicationRunner {

    private StockRepository stockRepository;
    private ProductRepository productRepository;
    private ProductOrderRepository productSaleRepository;

    @Autowired
    public DataLoader(StockRepository stockRepository, ProductRepository productRepository, ProductOrderRepository productSaleRepository) {
        this.stockRepository = stockRepository;
        this.productRepository = productRepository;
        this.productSaleRepository = productSaleRepository;
    }

    public void run(ApplicationArguments args) {
    	//Initial Data
    	
    	Product product1 = productRepository.save(new Product("vegetable-123", new Date()));
    	Product product2 = productRepository.save(new Product("car-54", new Date()));
    	Product product3 = productRepository.save(new Product("tire-11", new Date()));
    	Product product4 = productRepository.save(new Product("apple-45", new Date()));
    	Product product5 = productRepository.save(new Product("plane-99", new Date()));
    	productRepository.save(new Product("egg-131", new Date()));
    	
    	stockRepository.save(new Stock("000001", new Date(), 100, product1));
    	stockRepository.save(new Stock("121346", new Date(), 1200, product2));
    	stockRepository.save(new Stock("321346", new Date(), 11, product3));
    	stockRepository.save(new Stock("445566", new Date(), 1, product4));
    	stockRepository.save(new Stock("677657", new Date(), 1000, product5));
        
    	
    	//Today 
    	productSaleRepository.save(new ProductOrder(product1, 20, new Date()));
    	productSaleRepository.save(new ProductOrder(product1, 11, new Date()));
    	productSaleRepository.save(new ProductOrder(product1, 67, new Date()));
    	productSaleRepository.save(new ProductOrder(product2, 617, new Date()));
    	productSaleRepository.save(new ProductOrder(product2, 27, new Date()));
    	productSaleRepository.save(new ProductOrder(product3, 1, new Date()));
    	productSaleRepository.save(new ProductOrder(product3, 1, new Date()));
    	productSaleRepository.save(new ProductOrder(product5, 167, new Date()));
    	
    	//Last Month    	
    	Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 15);
    	productSaleRepository.save(new ProductOrder(product1, 5, cal.getTime()));
    	productSaleRepository.save(new ProductOrder(product2, 58, cal.getTime()));
    	productSaleRepository.save(new ProductOrder(product3, 85, cal.getTime()));
    	productSaleRepository.save(new ProductOrder(product4, 11, cal.getTime()));
    }
}