package com.commercetools.stock.controller;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.commercetools.stock.domain.Stock;
import com.commercetools.stock.exception.StockRequestExpiredException;
import com.commercetools.stock.service.StatisticService;
import com.commercetools.stock.service.StockService;
import com.commercetools.stock.util.JSONUtil;

@RestController
public class StockController {

	public static final Logger logger = LoggerFactory
			.getLogger(StockController.class);
	
	@Autowired
	private StockService stockService;

	@Autowired
	private StatisticService statisticService;

	@RequestMapping(value = "/updateStock", method = RequestMethod.POST)
	public ResponseEntity<?> updateStock(@RequestBody Stock newStock) {
		logger.info("Receiving new stock to update");
		try {
			stockService.update(newStock);
			return new ResponseEntity<Stock>(HttpStatus.CREATED);
		} catch (StockRequestExpiredException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/stock", method = RequestMethod.GET)
	public ResponseEntity<?> getStockByProduct(@RequestParam String productId) {
		logger.info("Request for a stock by product "+productId);
		Stock stock = stockService.findByProduct(productId);
		if (stock == null) {
			logger.info("Product not found");
			return new ResponseEntity<String>("Product not found",HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Map<String, Object>>(JSONUtil.getAsJson(stock, new Date()), HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/statistics", method = RequestMethod.GET)
	public ResponseEntity<?> getStatisticsByTime(@RequestParam String time) {
		logger.info("Request for statistics by time");
		try {
			return new ResponseEntity<Map<String, Object>>(
					JSONUtil.getAsJson(statisticService.generateByTopAvailableTopSell(time), new Date()),
					HttpStatus.ACCEPTED);
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NO_CONTENT);
		}

	}

}