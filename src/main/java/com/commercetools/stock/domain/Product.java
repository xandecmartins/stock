package com.commercetools.stock.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Product {
	
	@Id
	private String id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date creation;
	
	@OneToMany
	private List<ProductOrder> sales;
	
	@OneToOne
	private Stock stock;
	
	public Product() {
	}
	
	public Product(String id) {
		this.id = id;
	}
	
	public Product(String id, Date date) {
		this.id = id;
		this.creation = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public List<ProductOrder> getSales() {
		return sales;
	}

	public void setSales(List<ProductOrder> sales) {
		this.sales = sales;
	}
}
