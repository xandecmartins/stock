package com.commercetools.stock.domain;

import java.text.ParseException;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.commercetools.stock.util.DateUtil;

@Entity
public class Stock {

	@Id
	private String id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	private Integer quantity;

	@OneToOne
	private Product product;
	
	@Version
	private Long version;

	public Stock() {
	}
	
	public Stock(String id, Date timestamp, Integer quantity, Product product) {
		this.id = id;
		this.timestamp = timestamp;
		this.quantity = quantity;
		this.product = product;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) throws ParseException {
		this.timestamp = DateUtil.convertFromUTC(timestamp);
	}

	public Product getProduct() {
		return product;
	}

	public void setProductId(String productId) {
		this.product = new Product(productId);
	}
}
