package com.commercetools.stock.exception;


public class StockRequestExpiredException extends Exception {
	
	private static final long serialVersionUID = 3560017392855796071L;

	public StockRequestExpiredException(String message) {
		super(message);
	}
}
