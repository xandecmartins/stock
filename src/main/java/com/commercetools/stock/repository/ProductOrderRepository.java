package com.commercetools.stock.repository;
 
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.commercetools.stock.domain.ProductOrder;
 
@RepositoryRestResource
public interface ProductOrderRepository extends CrudRepository<ProductOrder, String> {

}