package com.commercetools.stock.repository;
 

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.commercetools.stock.domain.Product;
 
@RepositoryRestResource
public interface ProductRepository extends CrudRepository<Product, String> {
 
	@Query(value = "select p.id as productId, sum(ps.quantity) as itemsSold from Product p " +
            "inner join Product_Order ps " +
                "on p.id = ps.product_id " +
            " WHERE (ps.date between :begin AND :end) " +
            "group by p.id " +
            "order by itemsSold DESC limit :size", nativeQuery = true) 
	List<Object[]> findTopSelling(@Param("begin") Date begin, @Param("end") Date end, @Param("size") int size);
	
	
	
}