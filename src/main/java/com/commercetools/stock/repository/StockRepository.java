package com.commercetools.stock.repository;
 
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.commercetools.stock.domain.Product;
import com.commercetools.stock.domain.Stock;
 
@RepositoryRestResource
public interface StockRepository extends CrudRepository<Stock, String> {
 
	@Query("SELECT s FROM Stock s where s.product = :productId") 
	Stock findByProduct(@Param("productId") Product productId);
    
	@Query("SELECT s FROM Stock s ORDER BY s.quantity DESC") 
	List<Stock> findTopAvailable(Pageable pageable);
}