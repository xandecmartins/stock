package com.commercetools.stock.service;

import com.commercetools.stock.to.StatisticsTO;

public interface StatisticService {

	StatisticsTO generateByTopAvailableTopSell(String range);
}
