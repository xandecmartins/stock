package com.commercetools.stock.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import com.commercetools.stock.domain.Stock;
import com.commercetools.stock.repository.ProductRepository;
import com.commercetools.stock.repository.StockRepository;
import com.commercetools.stock.to.StatisticsTO;
import com.commercetools.stock.util.DateUtil;

public class StatisticServiceImpl implements StatisticService {

	@Autowired
	private StockRepository stockRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public StatisticsTO generateByTopAvailableTopSell(String time) {
		Date begin;
		Date end;
		
		if(time.equals("today")) {
			begin = DateUtil.getTodayMidnight();
			end = new Date();
		} else if (time.equals("lastMonth")) {
			begin = DateUtil.getFirstDayLastMonth();
			end = DateUtil.getLastDayLastMonth();
		} else {
			throw new IllegalArgumentException("The time "+time+" is invalid");
		}
		
		List<Stock> top3Avail = stockRepository.findTopAvailable(new PageRequest(0, 3));
		List<Object[]> top3Sell = productRepository.findTopSelling(begin, end,3);
		
		StatisticsTO retVal = new StatisticsTO();
		retVal.setRange(time);
		retVal.setTopAvailableProducts(top3Avail);
		retVal.setTopSellingProducts(top3Sell);
		return retVal;
		
		
	}

}
