package com.commercetools.stock.service;

import com.commercetools.stock.domain.Stock;
import com.commercetools.stock.exception.StockRequestExpiredException;

public interface StockService {

	Stock update(Stock newStock) throws StockRequestExpiredException;
	
	Stock findByProduct(String productId);
}
