package com.commercetools.stock.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.commercetools.stock.domain.Product;
import com.commercetools.stock.domain.Stock;
import com.commercetools.stock.exception.StockRequestExpiredException;
import com.commercetools.stock.repository.StockRepository;

public class StockServiceImpl implements StockService {

	@Autowired
	private StockRepository stockRepository;

	public Stock update(Stock newStock) throws StockRequestExpiredException {
		Stock currentStock = stockRepository.findByProduct(newStock.getProduct());

		// Business Rule - Checking Date Expired
		if (currentStock != null && newStock.getTimestamp() != null
				&& (currentStock.getTimestamp().after(newStock.getTimestamp())
						|| currentStock.getTimestamp().equals(newStock.getTimestamp()))) {
			throw new StockRequestExpiredException("outdated stock, because a newer stock was processed first");
		}
		
		if (currentStock != null) {
			// Removing the current stock
			stockRepository.delete(currentStock);
		}
		return stockRepository.save(newStock);
	}

	public Stock findByProduct(String productId) {
		return stockRepository.findByProduct(new Product(productId));
	}
}
