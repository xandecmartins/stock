package com.commercetools.stock.to;

import java.math.BigInteger;

public class ProductOrderTO {

	private String productId;
	private Integer itemsSold;
	
	public ProductOrderTO(Object[] values) {
		this.productId = (String)values[0];
		this.itemsSold = ((BigInteger)values[1]).intValue();
	}
	public String getProductId() {
		return productId;
	}
	public Integer getItemsSold() {
		return itemsSold;
	}
}
