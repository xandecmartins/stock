package com.commercetools.stock.to;

import java.io.Serializable;
import java.util.List;

import com.commercetools.stock.domain.Stock;

public class StatisticsTO implements Serializable{

	private static final long serialVersionUID = -6130248534007300588L;
	
	private String range;
	private List<Stock> topAvailableProducts;
	private List<Object[]> topSellingProducts;

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public List<Stock> getTopAvailableProducts() {
		return topAvailableProducts;
	}

	public void setTopAvailableProducts(List<Stock> topAvailableProducts) {
		this.topAvailableProducts = topAvailableProducts;
	}

	public List<Object[]> getTopSellingProducts() {
		return topSellingProducts;
	}

	public void setTopSellingProducts(List<Object[]> topSellingProducts) {
		this.topSellingProducts = topSellingProducts;
	}
	
}
