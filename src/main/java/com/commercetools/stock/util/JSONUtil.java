package com.commercetools.stock.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.commercetools.stock.domain.Stock;
import com.commercetools.stock.to.ProductOrderTO;
import com.commercetools.stock.to.StatisticsTO;

public class JSONUtil {

	public static Map<String, Object> getAsJson(Stock stock, Date requestTimestamp) {
		Map<String, Object> retVal = new LinkedHashMap<>();
		retVal.put("productId", stock.getProduct().getId());
		retVal.put("requestTimestamp", DateUtil.convertToUTC(requestTimestamp));

		Map<String, Object> stockMap = new LinkedHashMap<>();
		stockMap.put("id", stock.getId());
		stockMap.put("timestamp", DateUtil.convertToUTC(stock.getTimestamp()));
		stockMap.put("quantity", stock.getQuantity());

		retVal.put("stock", stockMap);
		return retVal;
	}

	public static Map<String, Object> getAsJson(StatisticsTO statisticsTO, Date requestTimestamp) {

		Map<String, Object> retVal = new LinkedHashMap<>();
		retVal.put("requestTimestamp", DateUtil.convertToUTC(requestTimestamp));
		retVal.put("range", statisticsTO.getRange());
		List<Map<String, Object>> top3AvailList = new ArrayList<>();
		for (Stock stock : statisticsTO.getTopAvailableProducts()) {
			Map<String, Object> top3AvailMap = new LinkedHashMap<>();
			top3AvailMap.put("id", stock.getId());
			top3AvailMap.put("timestamp", DateUtil.convertToUTC(stock.getTimestamp()));
			top3AvailMap.put("productId", stock.getProduct().getId());
			top3AvailMap.put("quantity", stock.getQuantity());
			top3AvailList.add(top3AvailMap);
		}
		retVal.put("topAvailableProducts", top3AvailList);

		List<ProductOrderTO> top3SellList = new ArrayList<>();
		for (Object[] objSell : statisticsTO.getTopSellingProducts()) {
			top3SellList.add(new ProductOrderTO(objSell));
		}
		retVal.put("topSellingProducts", top3SellList);
		return retVal;
	}

}
