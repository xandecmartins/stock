package com.commercetools.stock;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.commercetools.stock.domain.Product;
import com.commercetools.stock.domain.Stock;

public class BuildUtil {

	public static String getPostStockJson() {
		return "{\"id\":\"01234AAA\", \"quantity\":103311, \"productId\":\"pencil-111\", \"timestamp\":\"2017-12-16 22:54:01\" }";
	}
	
	public static Stock buildStock() {
		Product productStub = new Product("pencil-111", new Date());
		Stock stockStub = new Stock("112233", new Date(), 12000, productStub);
		return stockStub;
	}
	
	public static Stock buildStockNew() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 1);
		Product productStub = new Product("pencil-111", new Date());
		Stock stockStub = new Stock("94587",cal.getTime() , 102000, productStub);
		return stockStub;
	}
	
	public static Stock buildStockOld() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		Product productStub = new Product("pencil-111", new Date());
		Stock stockStub = new Stock("45677",cal.getTime() , 7000, productStub);
		return stockStub;
	}
	
	
	
	public static List<Stock> buildListStock(){
		List<Stock> retVal = new ArrayList<>();
		Product product1 = new Product("vegetable-123", new Date());
    	Product product2 = new Product("car-54", new Date());
    	Product product5 = new Product("plane-99", new Date());
    	
    	retVal.add(new Stock("121346", new Date(), 1200, product2));
    	retVal.add(new Stock("677657", new Date(), 1000, product5));
    	retVal.add(new Stock("000001", new Date(), 100, product1));
    	
    	return retVal;
	}
	
	public static List<Object[]> buildListSell(){
		List<Object[]> retVal = new ArrayList<>();
		retVal.add(new Object[] {"vegetable-123", new BigInteger("355")});
		retVal.add(new Object[] {"car-54", new BigInteger("200")});
		retVal.add(new Object[] {"egg-131", new BigInteger("100")});
		return retVal;
	}
	
}
