package com.commercetools.stock.controller;

import static org.mockito.Matchers.*;
import static com.commercetools.stock.BuildUtil.*;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.commercetools.stock.TestUtils;
import com.commercetools.stock.domain.Stock;
import com.commercetools.stock.exception.StockRequestExpiredException;
import com.commercetools.stock.service.StatisticService;
import com.commercetools.stock.service.StockService;
import com.commercetools.stock.to.StatisticsTO;

@RunWith(SpringRunner.class)
@WebMvcTest(StockController.class)
public class StockControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private StockService stockService;

	@MockBean
	private StatisticService statisticService;

	private static final String URL_STOCK = "/stock/";
	
	private static final String URL_STOCK_UPDATE = "/updateStock/";
	
	private static final String URL_STATISTICS = "/statistics/";

	@Test
	public void testGetStock() throws Exception {

		when(stockService.findByProduct(any(String.class))).thenReturn(buildStock());

		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.get(URL_STOCK + "?productId=pencil-111").accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

		int status = result.getResponse().getStatus();
		assertEquals("Incorrect Response Status", HttpStatus.ACCEPTED.value(), status);

		verify(stockService).findByProduct(any(String.class));

		@SuppressWarnings("unchecked")
		Map<String, Object> resultStock = TestUtils.jsonToObject(result.getResponse().getContentAsString(), Map.class);
		assertNotNull(resultStock);
		assertEquals("pencil-111", resultStock.get("productId"));
	}

	@Test
	public void testGetStockNotExist() throws Exception {

		// execute
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.get(URL_STOCK + "?productId=pencil-111").accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

		// verify
		int status = result.getResponse().getStatus();
		assertEquals("Incorrect Response Status", HttpStatus.NOT_FOUND.value(), status);

		// verify that service method was called once
		verify(stockService).findByProduct(any(String.class));

		assertEquals("Product not found", result.getResponse().getContentAsString());
	}
	
	@Test
	public void testUpdateStock() throws Exception {

		
		when(stockService.findByProduct(any(String.class))).thenReturn(buildStock());

		// execute
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(URL_STOCK_UPDATE).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8).content(getPostStockJson())).andReturn();

		// verify
		int status = result.getResponse().getStatus();
		assertEquals("Incorrect Response Status", HttpStatus.CREATED.value(), status);

		// verify that service method was called once
		verify(stockService).update(any(Stock.class));
	}
	
	@Test
	public void testUpdateStockExpired() throws Exception {

		when(stockService.update(any(Stock.class))).thenThrow(new StockRequestExpiredException("outdated stock, because a newer stock was processed first"));

		// execute
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(URL_STOCK_UPDATE).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8).content(getPostStockJson())).andReturn();

		// verify
		int status = result.getResponse().getStatus();
		assertEquals("Incorrect Response Status", HttpStatus.NO_CONTENT.value(), status);

		// verify that service method was called once
		verify(stockService).update(any(Stock.class));
		
		assertEquals("outdated stock, because a newer stock was processed first", result.getResponse().getContentAsString());
	}
	
	@Test
	public void testGetStatistic() throws Exception {

		StatisticsTO statisticsStub = new StatisticsTO();
		statisticsStub.setRange("today");
		statisticsStub.setTopAvailableProducts(buildListStock());
		statisticsStub.setTopSellingProducts(buildListSell());
		when(statisticService.generateByTopAvailableTopSell(any(String.class))).thenReturn(statisticsStub);

		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.get(URL_STATISTICS + "?time=today").accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

		int status = result.getResponse().getStatus();
		assertEquals("Incorrect Response Status", HttpStatus.ACCEPTED.value(), status);

		verify(statisticService).generateByTopAvailableTopSell(any(String.class));

		@SuppressWarnings("unchecked")
		Map<String, Object> resultStatistic = TestUtils.jsonToObject(result.getResponse().getContentAsString(), Map.class);
		assertNotNull(resultStatistic);
		assertEquals("today", resultStatistic.get("range"));
	}
	
	@Test
	public void testGetStatisticInvalidTime() throws Exception {

		when(statisticService.generateByTopAvailableTopSell(any(String.class))).thenThrow(new IllegalArgumentException("The time lastYear is invalid"));
		
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.get(URL_STATISTICS + "?time=lastYear").accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

		int status = result.getResponse().getStatus();
		assertEquals("Incorrect Response Status", HttpStatus.NO_CONTENT.value(), status);

		verify(statisticService).generateByTopAvailableTopSell(any(String.class));

		assertEquals("The time lastYear is invalid", result.getResponse().getContentAsString());
	}
	
	
	
}
