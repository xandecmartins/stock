package com.commercetools.stock.service;

import static com.commercetools.stock.BuildUtil.*;

import static org.junit.Assert.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.commercetools.stock.repository.ProductRepository;
import com.commercetools.stock.repository.StockRepository;
import com.commercetools.stock.to.StatisticsTO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StatisticsServiceTest {

	@InjectMocks
	private StatisticServiceImpl statisticService;
	
	@Mock
	private ProductRepository productRepository;
	
	@Mock
	private StockRepository stockRepository;
	
	@Test
	public void testGenerateStatisticToday() {
		when(stockRepository.findTopAvailable(any(Pageable.class))).thenReturn(buildListStock());
		when(productRepository.findTopSelling(any(Date.class),any(Date.class),any(Integer.class) )).thenReturn(buildListSell());
		
		StatisticsTO result = statisticService.generateByTopAvailableTopSell("today");
		
		verify(stockRepository).findTopAvailable(any(Pageable.class));
		verify(productRepository).findTopSelling(any(Date.class),any(Date.class),any(Integer.class));

		assertNotNull(result);
	}
	
	@Test
	public void testGenerateStatisticLastMonth() {
		when(stockRepository.findTopAvailable(any(Pageable.class))).thenReturn(buildListStock());
		when(productRepository.findTopSelling(any(Date.class),any(Date.class),any(Integer.class) )).thenReturn(buildListSell());
		
		StatisticsTO result = statisticService.generateByTopAvailableTopSell("lastMonth");
		
		verify(stockRepository).findTopAvailable(any(Pageable.class));
		verify(productRepository).findTopSelling(any(Date.class),any(Date.class),any(Integer.class));

		assertNotNull(result);

	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGenerateStatisticInvalidTime() {
		statisticService.generateByTopAvailableTopSell("lastYear");
	}
}
