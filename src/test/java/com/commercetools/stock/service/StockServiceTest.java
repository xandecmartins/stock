package com.commercetools.stock.service;

import static com.commercetools.stock.BuildUtil.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.commercetools.stock.domain.Product;
import com.commercetools.stock.domain.Stock;
import com.commercetools.stock.exception.StockRequestExpiredException;
import com.commercetools.stock.repository.StockRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StockServiceTest {

	@Mock
	private StockRepository stockRepository;
	
	@InjectMocks
	private StockServiceImpl stockService;
	
	@Test
	public void testFindStockByProduct() {
		when(stockRepository.findByProduct(any(Product.class))).thenReturn(buildStock());

		Stock stock = stockService.findByProduct("pencil-111");

		verify(stockRepository).findByProduct(any(Product.class));
		assertNotNull(stock);
		assertEquals("pencil-111", stock.getProduct().getId());

	}

	@Test
	public void testFindStockByProductNotExist() {
		Stock stock = stockService.findByProduct("pencil-111");

		verify(stockRepository).findByProduct(any(Product.class));
		assertNull(stock);
	}

	@Test
	public void testFindStockByProductInvalid() {
		Stock stock = stockService.findByProduct(null);

		verify(stockRepository).findByProduct(any(Product.class));
		assertNull(stock);

	}

	@Test
	public void testUpdateOk() throws StockRequestExpiredException {
			when(stockRepository.findByProduct(any(Product.class))).thenReturn(buildStock());
			when(stockRepository.save(any(Stock.class))).thenReturn(buildStockNew());
			
			Stock stock = stockService.update(buildStockNew());
			verify(stockRepository).save(any(Stock.class));
			assertNotNull(stock);
			assertEquals(buildStockNew().getId(), stock.getId());
	}
	
	@Test
	public void testUpdateProductNoPreviousStock() throws StockRequestExpiredException {
			when(stockRepository.findByProduct(any(Product.class))).thenReturn(null);
			when(stockRepository.save(any(Stock.class))).thenReturn(buildStockNew());
			
			Stock stock = stockService.update(buildStockNew());
			verify(stockRepository).save(any(Stock.class));
			assertNotNull(stock);
			assertEquals(buildStockNew().getId(), stock.getId());
	}
	
	@Test(expected=StockRequestExpiredException.class)
	public void testUpdateExpired() throws StockRequestExpiredException {
			when(stockRepository.findByProduct(any(Product.class))).thenReturn(buildStock());
			when(stockRepository.save(any(Stock.class))).thenReturn(buildStockNew());
			stockService.update(buildStockOld());
	}

}
